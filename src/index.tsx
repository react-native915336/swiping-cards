import React, {useEffect, useRef, useState} from 'react';
import images from './assets';
import {Animated, PanResponder, Pressable, Text, View} from 'react-native';
import styles from './index.style';
import {MAX_VELOCITY, MIN_VELOCITY, SWIPE_THRESHOLD} from './constants';
import {clamp} from './utils';

const items = [
  {
    id: 1,
    image: images.Cat1,
    text: 'Sari',
  },
  {
    id: 2,
    image: images.Cat2,
    text: 'Kara',
  },
  {
    id: 3,
    image: images.Cat3,
    text: 'Sari and Kara',
  },
];

const SwipingCards = () => {
  const [cards, setCards] = useState(items);
  const opacity = useRef(new Animated.Value(1)).current;
  const nextCard = useRef(new Animated.Value(0.9)).current;
  const pan = useRef(new Animated.ValueXY()).current;

  useEffect(() => {
    pan.setValue({x: 0, y: 0});
    opacity.setValue(1);
    nextCard.setValue(0.9);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cards]);

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([null, {dx: pan.x, dy: pan.y}], {
        useNativeDriver: false,
      }),
      onPanResponderRelease: (e, {dx, vx, vy}) => {
        let velocity;
        if (vx >= 0) {
          velocity = clamp(vx, MIN_VELOCITY, MAX_VELOCITY);
        } else {
          velocity = clamp(Math.abs(vx), MIN_VELOCITY, MAX_VELOCITY) * -1;
        }

        if (Math.abs(dx) > SWIPE_THRESHOLD) {
          Animated.decay(pan, {
            velocity: {x: velocity, y: vy},
            deceleration: 0.98,
            useNativeDriver: true,
          }).start(transitionToNextImage);
        } else {
          Animated.spring(pan, {
            toValue: {x: 0, y: 0},
            friction: 4,
            useNativeDriver: true,
          }).start();
        }
      },
    }),
  ).current;

  const transitionToNextImage = () => {
    Animated.parallel([
      Animated.timing(opacity, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true,
      }),
      Animated.spring(nextCard, {
        toValue: 1,
        friction: 4,
        useNativeDriver: true,
      }),
    ]).start(() => {
      setCards(prevState => prevState.slice(1));
    });
  };

  const rotate = pan.x.interpolate({
    inputRange: [-200, 0, 200],
    outputRange: ['-30deg', '0deg', '30deg'],
    extrapolate: 'clamp',
  });

  const animateOpacity = pan.x.interpolate({
    inputRange: [-200, 0, 200],
    outputRange: [0.5, 1, 0.5],
    extrapolate: 'clamp',
  });

  const animatedCardStyle = {
    transform: [
      {
        rotate,
      },
      ...pan.getTranslateTransform(),
    ],
    opacity: opacity,
  };

  const animatedImageStyle = {
    opacity: animateOpacity,
  };

  const onPressYes = () => {
    Animated.timing(pan.x, {
      toValue: SWIPE_THRESHOLD + 1,
      duration: 300,
      useNativeDriver: true,
    }).start(transitionToNextImage);
  };

  const onPressNo = () => {
    Animated.timing(pan.x, {
      toValue: -SWIPE_THRESHOLD - 1,
      duration: 300,
      useNativeDriver: true,
    }).start(transitionToNextImage);
  };

  const opacityOfNoLabel = pan.x.interpolate({
    inputRange: [-SWIPE_THRESHOLD, 0],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });

  const scaleOfNoLabel = pan.x.interpolate({
    inputRange: [-SWIPE_THRESHOLD, 0],
    outputRange: [1, 0.8],
    extrapolate: 'clamp',
  });

  const animatedNoStyle = {
    transform: [{scale: scaleOfNoLabel}, {rotate: '30deg'}],
    opacity: opacityOfNoLabel,
  };

  const opacityOfYesLabel = pan.x.interpolate({
    inputRange: [0, SWIPE_THRESHOLD],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  });

  const scaleOfYesLabel = pan.x.interpolate({
    inputRange: [0, SWIPE_THRESHOLD],
    outputRange: [0.8, 1],
    extrapolate: 'clamp',
  });

  const animatedYesStyle = {
    transform: [{scale: scaleOfYesLabel}, {rotate: '-30deg'}],
    opacity: opacityOfYesLabel,
  };

  return (
    <View style={styles.container}>
      <View style={styles.cardContainer}>
        {cards
          .slice(0, 2)
          .reverse()
          .map(({image, id, text}, index, cardItems) => {
            const isLastItem = index === cardItems.length - 1;
            const isSecondToLast = index === cardItems.length - 2;

            const panHandlers = isLastItem ? panResponder.panHandlers : {};

            const cardStyle = isLastItem ? animatedCardStyle : undefined;

            const imageStyle = isLastItem ? animatedImageStyle : undefined;

            const nextStyle = isSecondToLast
              ? {transform: [{scale: nextCard}]}
              : undefined;

            return (
              <Animated.View
                style={[styles.card, cardStyle, nextStyle]}
                key={id}
                {...panHandlers}>
                <Animated.Image
                  source={image}
                  style={[styles.image, imageStyle]}
                  resizeMode="cover"
                />
                <View style={styles.text}>
                  <Text style={styles.text}>{text}</Text>
                </View>
                {isLastItem && (
                  <Animated.View style={[styles.nope, animatedNoStyle]}>
                    <Text style={styles.nopeText}>NOPE</Text>
                  </Animated.View>
                )}
                {isLastItem && (
                  <Animated.View style={[styles.yup, animatedYesStyle]}>
                    <Text style={styles.yupText}>YUP</Text>
                  </Animated.View>
                )}
              </Animated.View>
            );
          })}
      </View>

      <View style={styles.buttonContainer}>
        <View style={[styles.outerContainer, styles.noButton]}>
          <Pressable onPress={onPressNo} style={styles.button}>
            <Text>NO</Text>
          </Pressable>
        </View>
        <View style={[styles.outerContainer, styles.yesButton]}>
          <Pressable onPress={onPressYes} style={styles.button}>
            <Text>YES</Text>
          </Pressable>
        </View>
      </View>
    </View>
  );
};

export default SwipingCards;
