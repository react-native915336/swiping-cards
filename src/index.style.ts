import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    width: 300,
    height: 300,
    position: 'absolute',
    borderRadius: 3,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 5,
    borderWidth: 1,
    borderColor: '#FFF',
  },
  image: {
    width: null,
    height: null,
    borderRadius: 2,
    flex: 3,
  },
  text: {
    flex: 1,
    backgroundColor: '#FFF',
    padding: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  outerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    shadowOpacity: 0.5,
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 10,
    paddingHorizontal: 20,
  },
  button: {
    width: 70,
    height: 70,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  yesButton: {
    shadowColor: '#00FF00',
  },
  noButton: {
    shadowColor: '#FF0000',
  },
  nope: {
    borderColor: '#FF0000',
    borderWidth: 2,
    position: 'absolute',
    padding: 20,
    borderRadius: 5,
    right: 20,
    top: 20,
    backgroundColor: '#FFF',
  },
  nopeText: {
    fontSize: 16,
    color: '#FF0000',
  },
  yup: {
    borderColor: '#00FF00',
    borderWidth: 2,
    position: 'absolute',
    padding: 20,
    borderRadius: 5,
    left: 20,
    top: 20,
    backgroundColor: '#FFF',
  },
  yupText: {
    fontSize: 16,
    color: '#00FF00',
  },
});

export default styles;
