export const SWIPE_THRESHOLD = 120;
export const MIN_VELOCITY = 3;
export const MAX_VELOCITY = 5;
